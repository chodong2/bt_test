#include "global_define.h"

#ifdef ENABLE_LOWPWR

void set_clk_low( void )
{
	if( g_str_bitflag.b2_power_mode == PWR_MODE_LOW )
	{
		return;
	}

	g_str_bitflag.b1_power_mode_change = 1;
	USART_Cmd(BT_UART, DISABLE);
    USART_ITConfig(BT_UART, USART_IT_RXNE, DISABLE);
	USART_ITConfig(BT_UART, USART_IT_TXE, DISABLE);
	
#ifdef ENABLE_AUDIO
	audio_rx_stop();
#endif //ENABLE_AUDIO

	//TM_RCC_PLL_Off();
	//TIM_SetAutoreload(MONITOR_TIM_BASE, SystemCoreClock / (10000 - 1));
	g_str_bitflag.b2_power_mode = PWR_MODE_LOW;
	
#ifdef ENABLE_AUDIO
	audio_rx_start();
#endif //ENABLE_AUDIO

	bt_uart_config();
	USART_Cmd(BT_UART, ENABLE);
    USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
	USART_ITConfig(BT_UART, USART_IT_TXE, ENABLE);
	
	g_str_bitflag.b1_power_mode_change = 0;
}

void set_clk_high( void )
{
	if( g_str_bitflag.b2_power_mode == PWR_MODE_HIGH )
	{
		return;
	}

	g_str_bitflag.b1_power_mode_change = 1;

#ifdef ENABLE_AUDIO
	audio_rx_stop();
#endif
	
	USART_Cmd(BT_UART, DISABLE);
    USART_ITConfig(BT_UART, USART_IT_RXNE, DISABLE);
	USART_ITConfig(BT_UART, USART_IT_TXE, DISABLE);
	
	TM_RCC_PLL_t tmpPLL;
    tmpPLL.PLLM = 16;
	tmpPLL.PLLQ = 4;
	tmpPLL.PLLN = 400;
	tmpPLL.PLLP = 4;  
	TM_RCC_SetPLL(&tmpPLL);
	TIM_SetAutoreload(MONITOR_TIM_BASE, SystemCoreClock / (10000 - 1));
	g_str_bitflag.b2_power_mode = PWR_MODE_HIGH;

#ifdef ENABLE_AUDIO
	audio_rx_start();
#endif

	bt_uart_config();
	USART_Cmd(BT_UART, ENABLE);
    USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
	USART_ITConfig(BT_UART, USART_IT_TXE, ENABLE);
	
	g_str_bitflag.b1_power_mode_change = 0;
}

void set_sleepMode( void )
{
#ifdef ENABLE_PWM
	pwm_pulse_off();
#endif
	//goto sleep
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);

	/* After wakeup, call system init to enable PLL as clock source */
	SystemInit();
}

#endif //ENABLE_LOWPWR
#include "global_define.h"

#include <stdio.h>
#include <string.h>

#ifdef ENABLE_IS2063
#ifdef ENABLE_OTA

typedef enum {
	OTA_PARSER_INITIAL,
	OTA_PARSER_INDEX,
	OTA_PARSER_DATAFLAG,
	OTA_PARSER_DATA,
	OTA_PARSER_END,
} OTA_PARSER_MODE;

enum {
	OTA_TRIGGER				= 0,
	OTA_VERSION				= 1,
	OTA_SIZE				= 2,
	OTA_DATA				= 3,
	OTA_DATA_END			= 4,
	OTA_END					= 5,
};

static uint8_t data_buff[17];

static OTA_PARSER_MODE OTA_ParserState;

void ota_parser( uint8_t* u8_data, uint8_t u8_size )
{
	uint8_t u8_current_data = u8_data[0];
	uint8_t u8_index = 0;
	
	OTA_ParserState = OTA_PARSER_INITIAL;
	
	for( int i = 0; i < u8_size; i++ )
	{
		u8_current_data = u8_data[i];
		
		switch(OTA_ParserState)
		{
		case OTA_PARSER_INITIAL :
			if(u8_current_data == '@')
			{
				OTA_ParserState = OTA_PARSER_INDEX;
			}
			break;
			
		case OTA_PARSER_INDEX:
			u8_index = u8_current_data-'0';
			
			if( u8_current_data == '0' )
			{
				receive_ota( &u8_data[i], u8_index );
				OTA_ParserState = OTA_PARSER_INITIAL;
				
				return;
			}
			else
			{
				OTA_ParserState = OTA_PARSER_DATAFLAG;
			}
			break;
		
		case OTA_PARSER_DATAFLAG :
			if(u8_current_data == '@')
			{
				OTA_ParserState = OTA_PARSER_DATA;
			}
			break;

		case OTA_PARSER_DATA :
			OTA_ParserState = OTA_PARSER_END;
			break;
		  
		default :
			OTA_ParserState = OTA_PARSER_INITIAL;
			break;

		}
		
		if( OTA_ParserState == OTA_PARSER_END )
		{
			receive_ota( &u8_data[i], u8_index );
			OTA_ParserState = OTA_PARSER_INITIAL;
			
			return;
		}

	}
	
}

void receive_ota( uint8_t* u8_data, uint8_t u8_index )
{
	uint16_t u16_size = 0;
	//uint16_t u16_mode = 0;
	
	switch( u8_index )
	{
	case OTA_TRIGGER :
		g_str_ota.ui16_state = OTA_STATE_INITIAL;
		break;
	case OTA_VERSION :	u16_size = 6;
		if( g_str_ota.ui16_state == OTA_STATE_INITIAL)
		{
			g_str_ota.ui16_state = OTA_STATE_CHECK_VERSION;
		}
		break;
	case OTA_SIZE :	u16_size = 6;
		if( g_str_ota.ui16_state == OTA_STATE_CHECK_VERSION )
		{
			g_str_ota.ui16_state = OTA_STATE_CHECK_SIZE;
		}
		break;
	case OTA_DATA :		u16_size = 17;
		if( ( g_str_ota.ui16_state == OTA_STATE_READY ) )//|| ( g_str_ota.ui16_state == OTA_STATE_CHECK_SIZE ) )
		{
			memcpy(data_buff,u8_data,u16_size);
			g_str_ota.ui16_state = OTA_STATE_UPDATE;
		}
		break;
	case OTA_DATA_END :		u16_size = 1;
		if( g_str_ota.ui16_state == OTA_STATE_READY )
		{
			g_str_ota.ui16_state = OTA_STATE_DATA_END;
		}
		break;
	case OTA_END :		u16_size = 1;
		if( g_str_ota.ui16_state == OTA_STATE_DATA_END )
		{
			g_str_ota.ui16_state = OTA_STATE_END;
		}
		break;
	default : break;
	}
  
}

static ota_state_t OTA_TaskState = OTA_STATE_NONE;

void OTA_Task( void )
{
#if 1 //ota_ready
	static uint32_t Address = FLASH_ADDR_UPDATE_APP;
	static uint16_t ui16_offset = 0;
	static uint16_t ui16_retry = 0;
#endif
	
	if( OTA_TaskState == OTA_STATE_NONE )
	{
		OTA_TaskState = OTA_STATE_INITIAL;
		Address = FLASH_ADDR_UPDATE_APP-32;
		ui16_offset = 1;
	}

	switch(OTA_TaskState)
	{
	case OTA_STATE_INITIAL :
		//send i'm ready, version check??
		uint8_t byte[] = "@1@0";
		BT_SendSPPData(byte, 4, 0);
		OTA_TaskState = OTA_STATE_CHECK_VERSION;
		break;
	case OTA_STATE_CHECK_VERSION:
		if( g_str_ota.ui16_state == OTA_STATE_CHECK_VERSION )
		{
			//version check
			//if ok
			//send version ok i'm ready
		  	uint8_t byte[] = "@2@0";
			BT_SendSPPData(byte, 4, 0);
			OTA_TaskState = OTA_STATE_CHECK_SIZE;
		  
		  	//else don't need
			//send don't need update
		}
		break;
	case OTA_STATE_CHECK_SIZE :
		if( g_str_ota.ui16_state == OTA_STATE_CHECK_SIZE )
		{
			//size check
			//if ok
			//send version ok i'm ready
		  	uint8_t byte[] = "@3@0";
			BT_SendSPPData(byte, 4, 0);
			OTA_TaskState = OTA_STATE_UPDATE;
			g_str_ota.ui16_state = OTA_STATE_READY;
			
#if  1//ota ready

			FLASH_Unlock();
			FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
	              FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR); 
		  
			for (int i = ERASE_START_SECTOR; i < ERASE_END_SECTOR; i += 8)
			{
				/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
				be done by word */ 
				if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
				{ 
					/* Error occurred while sector erase. 
					User can add here some code to deal with this error  */
					while (1)
					{
					}
				}
			}
			FLASH_Lock();
#endif
		  	//else don't need
			//send fail
		}
		break;
	case OTA_STATE_UPDATE :
		if( g_str_ota.ui16_state == OTA_STATE_UPDATE )
		{
		  	//write flash
		  	uint8_t byte[10];
			uint8_t ui8_chksum = 0;
			
			//ui16_offset = 0;
			sprintf(byte,"@3@%d",ui16_offset);
			
			if( ui16_offset < 10 )
			{
				BT_SendSPPData(byte, 4, 0);
			}
			else if ( ui16_offset < 100 )
			{
				BT_SendSPPData(byte, 5, 0);
			}
			else if ( ui16_offset < 1000 )
			{
				BT_SendSPPData(byte, 6, 0);
			}
			else if ( ui16_offset < 10000 )
			{
				BT_SendSPPData(byte, 7, 0);
			}
			
			OTA_TaskState = OTA_STATE_READY;
			g_str_ota.ui16_state = OTA_STATE_READY;
#if 1 //ota_ready
			
			for(int i = 0 ; i < MAX_GET_DATA_SIZE ; i++)
			{
				ui8_chksum += data_buff[i];
			}
			
			ui8_chksum = ~ui8_chksum + 1;
			
			if( ui8_chksum == data_buff[16] )
			{
				ui16_offset++;

				FLASH_Unlock();
				
				for(int i = 0 ; i < MAX_GET_DATA_SIZE ; i++)
				{
					if (FLASH_ProgramByte(Address, data_buff[i]) == FLASH_COMPLETE)
					{
						Address++;
					}
					else
					{ 
						/* Error occurred while writing data in Flash memory. 
						User can add here some code to deal with this error */
						FLASH_Lock();
						return;// FLASH_ERROR_WRITE;
					}
				}
				
				FLASH_Lock();
				ui16_retry = 0;
			}
			else
			{
				ui16_retry++;
			}
			
			if( ui16_retry > 10 )
			{
				OTA_TaskState = OTA_STATE_FAIL;
				break;
			}
#endif
		}
		else if(g_str_ota.ui16_state == OTA_STATE_DATA_END)
		{
			//finish flash
		  	uint8_t byte[] = "@4@0";
			BT_SendSPPData(byte, 4, 0);
			OTA_TaskState = OTA_STATE_DATA_END;
		}
		  
		break;
	case OTA_STATE_DATA_END :
		OTA_TaskState = OTA_STATE_SUCCESS;
		break;
	case OTA_STATE_SUCCESS :
		{
			uint8_t byte[] = "@6@0";
			BT_SendSPPData(byte, 4, 0);
			OTA_TaskState = OTA_STATE_END;
			g_str_ota.ui16_state = OTA_STATE_SUCCESS;
		}
		break;
	case OTA_STATE_FAIL :
		{
			uint8_t byte[] = "@7@0";
			BT_SendSPPData(byte, 4, 0);
			OTA_TaskState = OTA_STATE_END;
			g_str_ota.ui16_state = OTA_STATE_FAIL;
		}
		break;
	case OTA_STATE_END :
		
		if( g_str_ota.ui16_state == OTA_STATE_SUCCESS )
		{
			JumpToUpdateApp();
		}
		
		g_str_ota.ui16_state = OTA_STATE_NONE;
		OTA_TaskState = OTA_STATE_NONE;
		
	  	break;
	case OTA_STATE_READY :
		if( ( g_str_ota.ui16_state == OTA_STATE_UPDATE ) || ( g_str_ota.ui16_state == OTA_STATE_DATA_END ) )
		{
			OTA_TaskState = OTA_STATE_UPDATE;
		}
		break;
	default : break;
	}
}

typedef  void (*pFunction)(void);
pFunction Jump_To_Application;

void JumpToUserApp(uint32_t jump_addr)
{
  	uint32_t JumpAddress;
	/* Jump to user application */
	JumpAddress = *(__IO uint32_t*) (jump_addr + 4);
	Jump_To_Application = (pFunction) JumpAddress;

	/* Initialize user application's Stack Pointer */
	Jump_To_Application();
}

void JumpToUpdateApp( void )
{
  	uint32_t jump_addr;

	jump_addr = FLASH_ADDR_UPDATE_APP;//*(__IO uint32_t*) (FLASH_ADDR_JUMP_APP);
  
	RCC_DeInit();
	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;
	
	//__set_PRIMASK(1);
	__set_MSP(*(__IO uint32_t*)jump_addr);
	
	JumpToUserApp(jump_addr);
}


#endif //ENABLE_OTA
#endif //ENABLE_IS2063
/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#ifndef _INV_DEFINES_H_
#define _INV_DEFINES_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "dmp3Default_20648.h"
#include "i2c.h"

#include "inv_mems_hw_config.h"
#include "pedometer.h"

#include "invn_types.h"
#include "inv_mems.h"
#include "inv_mems_base_control.h"
#include "inv_mems_base_driver.h"
#include "inv_mems_defines.h"
#include "inv_mems_interface_mapping.h"
#include "inv_mems_mpu_fifo_control.h"
#include "inv_mems_transport.h"
#include "misc.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h" 
#include "stm32f4xx_i2c.h" 
#include "stm32f4xx_rcc.h"

#include "global_define.h"

extern uint32_t g_ui32_pedometer_timer;
extern uint32_t g_ui32_pedometer_send_cnt;
extern uint32_t g_ui32_pedometer_call_app;
extern uint32_t g_ui32_date;
extern uint16_t g_ui16_hour;
extern uint32_t g_ui32_mcu_hour;
extern uint32_t g_ui32_app_date;
extern uint16_t g_ui16_app_hour;

typedef struct pedometer_struct_
{
        uint32_t ui32_still_step;
        uint32_t ui32_run_step;
        uint32_t ui32_walk_step;
        uint32_t ui32_still_time;
        uint32_t ui32_run_time;
        uint32_t ui32_walk_time;
        
        uint32_t ui32_total_still_step;
        uint32_t ui32_total_run_step;
        uint32_t ui32_total_walk_step;
        uint32_t ui32_total_still_time;
        uint32_t ui32_total_run_time;
        uint32_t ui32_total_walk_time;
}pedometer_struct_t;
extern pedometer_struct_t g_str_pedo;

#endif
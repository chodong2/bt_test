#ifndef TIMER_EVENT_H
#define TIMER_EVENT_H

#define MONITOR_TIM_IRQ     TIM4_IRQn
#define MONITOR_TIM_RCC     RCC_APB1Periph_TIM4
#define MONITOR_TIM_BASE    TIM4

extern volatile uint32_t g_ui32_10ms_count;
extern volatile uint32_t g_ui32_1ms_count;

void monitor_init( void );
void monitor_timer_config( void );
void monitor_timer_stop( void );
void monitor_timer_start( void );
extern void delay_10ms(uint32_t nTime);
#define monitor_timer_isr                  TIM4_IRQHandler

#endif //TIMER_EVENT_H
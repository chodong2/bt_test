#include "global_define.h"

#ifdef ENABLE_UART

void uart_init(void)
{
	bt_uart_config();
}

/* UART Configuration
* TxD  PA2
* RxD  PA3
* async. mode
* baudrate 115200
* Word Length = 8 Bits
* one Stop Bit
* No parity
* Hardware flow control disabled (RTS and CTS signals)
* Receive and transmit enabled
*/
void bt_uart_config(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(BT_UART_GPIO_CLK, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(BT_UART_CLK, ENABLE);

	/* Connect USART pins to AF7 */
	GPIO_PinAFConfig(BT_UART_GPIO_PORT, BT_UART_TX_SOURCE, BT_UART_AF);
	GPIO_PinAFConfig(BT_UART_GPIO_PORT, BT_UART_RX_SOURCE, BT_UART_AF);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

	GPIO_InitStructure.GPIO_Pin = BT_UART_TX_PIN;
	GPIO_Init(BT_UART_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = BT_UART_RX_PIN;
	GPIO_Init(BT_UART_GPIO_PORT, &GPIO_InitStructure);

	/* USART2 configuration ----------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = BT_UART_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(BT_UART, &USART_InitStructure);

	/* NVIC configuration */

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = BT_UART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable USART */
	USART_Cmd(BT_UART, ENABLE);
    USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
}

void bt_uart_isr(void)
{
 	
#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif

#ifdef ENABLE_IS2063
	if (USART_GetITStatus(BT_UART, USART_IT_TXE) == SET)
	{
		if( g_str_bt.ui16_tx_cnt < UART_BUFFERSIZE )
		{
			USART_SendData(BT_UART, g_str_bt.ui8_tx_buf[g_str_bt.ui16_tx_tail++]);
			
			if( g_str_bt.ui16_tx_tail == UART_BUFFERSIZE )
			{
				g_str_bt.ui16_tx_tail = 0;  
			}
			
			g_str_bt.ui16_tx_cnt ++;
			UART_TransferNextByte();
		}
		else
		{
			g_str_bt.ui16_tx_flag = 0;
			USART_ITConfig(BT_UART, USART_IT_TXE, DISABLE);
			UART_TransferNextByte();
		}
		
		USART_ClearITPendingBit(BT_UART, USART_IT_TXE);
	}
	else 
#endif //ENABLE_IS2063
	/* USART in Receiver mode */
	if (USART_GetITStatus(BT_UART, USART_IT_RXNE) == SET)
	{
		/* Receive Transaction data */
		
		g_str_bt.ui8_rx_buf[g_str_bt.ui16_rx_head++] = USART_ReceiveData(BT_UART);
		
		if( g_str_bt.ui16_rx_head == UART_BUFFERSIZE )
		{
			g_str_bt.ui16_rx_head = 0;
		}
		
		g_str_bt.ui16_rx_cnt++;
		
		USART_ClearITPendingBit(BT_UART, USART_IT_RXNE);
	}

}

uint8_t uart_read_byte( void )
{
	uint8_t ui8_data = 0;
	
	ui8_data = g_str_bt.ui8_rx_buf[g_str_bt.ui16_rx_tail++];
	
	if( g_str_bt.ui16_rx_tail == UART_BUFFERSIZE )
	{
		g_str_bt.ui16_rx_tail = 0;
	}
	g_str_bt.ui16_rx_cnt--;

	return ui8_data;
}

void uart_send_byte( uint8_t ui8_byte )
{
  	while(0 == g_str_bt.ui16_tx_cnt)
    {
    }
  
	if( g_str_bt.ui16_tx_flag == 0)
	{
		USART_SendData(BT_UART, ui8_byte);
		while (USART_GetFlagStatus(BT_UART, USART_FLAG_TXE) == RESET)
		{}
	}
	else
	{
		g_str_bt.ui16_tx_flag = 0;
		USART_ITConfig(BT_UART, USART_IT_TXE, DISABLE);
		g_str_bt.ui8_tx_buf[g_str_bt.ui16_tx_head++] = ui8_byte;
		
		if( g_str_bt.ui16_tx_head == UART_BUFFERSIZE )
		{
			g_str_bt.ui16_tx_head = 0;
		}
		g_str_bt.ui16_tx_cnt--;
	}
	
	g_str_bt.ui16_tx_flag = 1;
	USART_ITConfig(BT_UART, USART_IT_TXE, ENABLE);

}

#endif //ENABLE_UART

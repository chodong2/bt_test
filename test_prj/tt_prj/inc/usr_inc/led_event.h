#ifndef LED_EVENT_H
#define LED_EVENT_H

#define LED_GPIO_1      GPIO_Pin_5
#define LED_GPIO_2      GPIO_Pin_7
#define LED_GPIO_3      GPIO_Pin_8
#define LED_GPIO_4      GPIO_Pin_9
#define LED_GPIO_5      GPIO_Pin_10
#define LED_GPIO_6      GPIO_Pin_11
#define LED_GPIO_7      GPIO_Pin_12
#define LED_ALL         ( LED_GPIO_1 | LED_GPIO_2 | LED_GPIO_3 | LED_GPIO_4 | LED_GPIO_5 | LED_GPIO_6 | LED_GPIO_7 )

#define LED_GPIO_PORT   GPIOA
#define LED_GPIO_RCC    RCC_AHB1Periph_GPIOA
#define LED_TIM_IRQ     TIM5_IRQn
#define LED_TIM_RCC     RCC_APB1Periph_TIM5
#define LED_TIM_BASE    TIM5

void led_init( void );
void led_gpio_config( void );
void led_timer_config( void );

void led_blink( void );
void led_timer_stop( void );
void led_timer_start( uint16_t ui16_led_cnt );

#define led_timer_isr                  TIM5_IRQHandler

#endif //LED_EVENT_H
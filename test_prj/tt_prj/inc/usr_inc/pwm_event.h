#ifndef PWM_EVENT_H
#define PWM_EVENT_H

#ifndef ENABLE_DBG
	#define PWM_TIM1_GPIO_2		GPIO_Pin_9
	#define PWM_TIM1_GPIO_3		GPIO_Pin_10
#endif //ENABLE_DBG

#define PWM_TIM1_GPIO_4		GPIO_Pin_11


#ifndef ENABLE_DBG
	#define PWM_LED1_GPIO		GPIO_Pin_10
#endif //ENABLE_DBG

#define PWM_LED2_GPIO		GPIO_Pin_11

#define PWM_TIM1_GPIO_SOURCE1		GPIO_PinSource8

#ifndef ENABLE_DBG
	#define PWM_TIM1_GPIO_SOURCE2		GPIO_PinSource9
	#define PWM_TIM1_GPIO_SOURCE3		GPIO_PinSource10
#endif //ENABLE_DBG

#define PWM_TIM1_GPIO_SOURCE4		GPIO_PinSource11

#ifndef ENABLE_DBG
	#define PWM_LED1_GPIO_SOURCE		GPIO_PinSource10
#endif //ENABLE_DBG

#define PWM_LED2_GPIO_SOURCE		GPIO_PinSource11

#ifdef ENABLE_DBG
	#define PWM_TIM1         			(  PWM_TIM1_GPIO_1 | PWM_TIM1_GPIO_4 )
#else
	#define PWM_TIM1         			(  PWM_TIM1_GPIO_2 | PWM_TIM1_GPIO_3 | PWM_TIM1_GPIO_4 )
#endif //ENABLE_DBG

#define PWM_TIM1_GPIO_PORT		GPIOA
#define PWM_TIM1_GPIO_RCC		RCC_APB2Periph_TIM1
#define PWM_TIM1_GPIO_CLK		RCC_AHB1Periph_GPIOA

#define PWM_TIM3_GPIO_1			GPIO_Pin_7
#define PWM_TIM3_GPIO_2			GPIO_Pin_0
#define PWM_TIM3_GPIO_3			GPIO_Pin_1

#define PWM_LED3_GPIO		GPIO_Pin_7
#define PWM_LED4_GPIO		GPIO_Pin_0
#define PWM_LED5_GPIO		GPIO_Pin_1


#define PWM_TIM3_GPIO_SOURCE1		GPIO_PinSource7
#define PWM_TIM3_GPIO_SOURCE2		GPIO_PinSource0
#define PWM_TIM3_GPIO_SOURCE3		GPIO_PinSource1

#define PWM_LED3_GPIO_SOURCE		GPIO_PinSource7
#define PWM_LED4_GPIO_SOURCE		GPIO_PinSource0
#define PWM_LED5_GPIO_SOURCE		GPIO_PinSource1

#define PWM_TIM3_PART1				PWM_TIM3_GPIO_1
#define PWM_TIM3_PART2				( PWM_TIM3_GPIO_2 | PWM_TIM3_GPIO_3 )

#define PWM_TIM3_GPIO_PORT1		GPIOA
#define PWM_TIM3_GPIO_PORT2		GPIOB
#define PWM_TIM3_GPIO_RCC		RCC_APB1Periph_TIM3

#define PWM_GPIO_CLK			(RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOB)

#define PWM_HAPTIC_GPIO_PORT	GPIOA

#ifndef ENABLE_DBG
	#define PWM_LED1_GPIO_PORT	GPIOA
#endif //ENABLE_DBG

#define PWM_LED2_GPIO_PORT	GPIOA
#define PWM_LED3_GPIO_PORT	GPIOA
#define PWM_LED4_GPIO_PORT	GPIOB
#define PWM_LED5_GPIO_PORT	GPIOB

#define PWM_HAPTIC_CCR	TIM1->CCR1

#ifndef ENABLE_DBG
	#define PWM_LED1_CCR	TIM1->CCR3
#endif //ENABLE_DBG

#define PWM_LED2_CCR	TIM1->CCR4
#define PWM_LED3_CCR	TIM3->CCR2
#define PWM_LED4_CCR	TIM3->CCR3
#define PWM_LED5_CCR	TIM3->CCR4


void pwm_init( void );
void pwm_config_gpio( void );

void pwm_config_tim1_output( void );
void pwm_config_tim3_output( void );

void pwm_pulse_off( void );

void pwm_pulse_led_all(uint16_t ui16_pulse);

void pwm_pulse_led1(uint16_t ui16_pulse);
void pwm_pulse_led2(uint16_t ui16_pulse);
void pwm_pulse_led3(uint16_t ui16_pulse);
void pwm_pulse_led4(uint16_t ui16_pulse);
void pwm_pulse_led5(uint16_t ui16_pulse);

#ifdef ENABLE_TEST
void pwm_pulse_test(uint16_t ui16_pulse);
#endif //ENABLE_TEST

#endif //PWM_EVENT_H
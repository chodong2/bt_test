/******************************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PICmicro(r) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PICmicro Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
********************************************************************/
#ifndef BT_APP_H
#define BT_APP_H

#include <stdbool.h>
#include <stdint.h>

void BTAPP_Init( void );
void BTAPP_Task( void );
void BTAPP_Timer1MS_event( void );
// @ request define
enum {
    BT_REQ_NONE = 0,
    BT_REQ_SYSTEM_ON,   //BM64 power on request(outside application must control RESET and MFB timing, then call this request)
    BT_REQ_SYSTEM_OFF,  //BM64 power off request(after call this, outside application must control RESET and MFB)
};
void BTAPP_TaskReq(uint8_t request);

// @ event define
enum {
    BT_EVENT_NONE = 0,

    BT_EVENT_NSPK_STATUS,
    BT_EVENT_LINE_IN_STATUS,
    BT_EVENT_A2DP_STATUS,
    BT_EVENT_CALL_STATUS_CHANGED,

    BT_EVENT_HFP_CONNECTED,
    BT_EVENT_HFP_DISCONNECTED,
    BT_EVENT_A2DP_CONNECTED,
    BT_EVENT_A2DP_DISCONNECTED,
    BT_EVENT_AVRCP_CONNECTED,
    BT_EVENT_AVRCP_DISCONNECTED,
    BT_EVENT_SPP_CONNECTED,
    BT_EVENT_IAP_CONNETED,
    BT_EVENT_SPP_IAP_DISCONNECTED,
    BT_EVENT_ACL_CONNECTED,
    BT_EVENT_ACL_DISCONNECTED,
    BT_EVENT_SCO_CONNECTED,
    BT_EVENT_SCO_DISCONNECTED,
    BT_EVENT_MAP_CONNECTED,
    BT_EVENT_MAP_DISCONNECTED,

    BT_EVENT_SYS_POWER_ON,
    BT_EVENT_SYS_POWER_OFF,
    BT_EVENT_SYS_STANDBY,
    BT_EVENT_SYS_PAIRING_START,
    BT_EVENT_SYS_PAIRING_OK,
    BT_EVENT_SYS_PAIRING_FAILED,

    BT_EVENT_LINKBACK_SUCCESS,
    BT_EVENT_LINKBACK_FAILED,

#if 1 //SH_ADD
	BT_EVENT_READ_LINK_STATUS,
	BT_EVENT_REPORT_TYPE_CODEC,
#endif
	
    BT_EVENT_BD_ADDR_RECEIVED,
    BT_EVENT_PAIR_RECORD_RECEIVED,
    BT_EVENT_LINK_MODE_RECEIVED,

    BT_EVENT_PLAYBACK_STATUS_CHANGED,
    BT_EVENT_AVRCP_VOLUME_CTRL,
    BT_EVENT_AVRCP_ABS_VOLUME_CHANGED,
    BT_EVENT_HFP_VOLUME_CHANGED,
    
    NSPK_EVENT_SYNC_POWER_OFF,
    NSPK_EVENT_SYNC_VOL_CTRL,
    NSPK_EVENT_SYNC_INTERNAL_GAIN,
    NSPK_EVENT_SYNC_ABS_VOL,
    NSPK_EVENT_CHANNEL_SETTING,
    NSPK_EVENT_ADD_SPEAKER3,
#if 1 //SH_ADD
	
	BT_EVENT_BTM_BATT_LEVEL,
	BT_EVENT_BTM_CHG_TYPE,
	LE_ANCS_REPORT,
#endif
    LE_STATUS_CHANGED,
    LE_ADV_CONTROL_REPORT,
    LE_CONNECTION_PARA_REPORT,
    LE_CONNECTION_PARA_UPDATE_RSP,
    GATT_ATTRIBUTE_DATA,
    
    PORT0_INPUT_CHANGED,
    PORT1_INPUT_CHANGED,
    PORT2_INPUT_CHANGED,
    PORT3_INPUT_CHANGED,
};

enum {
    BT_STATUS_NONE,
    BT_STATUS_OFF,
    BT_STATUS_ON,
    BT_STATUS_READY
};

uint8_t BTAPP_GetStatus(void);

void BTAPP_EventHandler(uint8_t event, uint16_t para, uint8_t* para_full);

void BTAPP_EnterBTPairingMode( void );
void BTAPP_ResetEEPROMtoDefault( void );
void BTAPP_PlayNextSong( void );
void BTAPP_PlayPreviousSong( void );
void BTAPP_PlayPause( void );
void BTAPP_StartFastForward( void );
void BTAPP_StartFastRewind( void );
void BTAPP_CancelForwardOrRewind( void );
void BTAPP_BroadcastBtnLongPress(void);
void BTAPP_BroadcastBtnDbClick(void);
void BTAPP_ExitBroadcastRegisterMode( void );
void BTAPP_GroupSpeakerSoundSync( void );
void BTAPP_NSPKBtnLongPress( void );
void BTAPP_NSPKBtnDbClick( void );
void BTAPP_NSPKBtnShortPress( void );
void BTAPP_CallEventShort( void );
void BTAPP_CallEventLong(void);
void BTAPP_VolUp( void );
void BTAPP_VolDown( void );

bool getI2SAuxInJumper( void );
bool getDatabase3Jumper( void );
//SPP data exchange
void BT_SPPBuffClear( void );
bool BTAPP_SendDataOverSPP(uint8_t* addr, uint32_t size);
bool BTAPP_SendCharOverSPP(uint8_t byte);
bool BT_AddBytesToSPPBuff(uint8_t* data, uint8_t size);
bool BT_AddBytesToSPPBuffFromBeginning(uint8_t* data, uint8_t size, bool data_True_Command_False)  ;      //test
void SPPTask(void);                                 //test
uint8_t IsSPPTaskIdle(void);
bool BT_ReadOneByteFromSPPBuff( uint8_t* byte );
void BT_SaveLocalBDAddress(uint8_t* address);
void BT_SaveMasterBDAddress(uint8_t* address);
bool BT_CustomerGATT_AttributeData(uint8_t attributeIndex, uint8_t* attributeData, uint8_t attributeDataLength);

extern uint16_t BTAPP_timer1ms;
#endif

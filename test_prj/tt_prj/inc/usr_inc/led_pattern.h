#ifndef _LED_PATTERN_H_
#define _LED_PATTERN_H_

void pattern_batt_level( void );
void pattern_app_noti( void );
void pattern_bt_conn( void );
void pattern_bt_disconn( void );

void pattern_power_on( void );

void pattern_incoming_call( void );

#endif //_LED_PATTERN_H_
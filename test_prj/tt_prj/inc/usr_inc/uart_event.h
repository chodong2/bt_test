#ifndef UART_EVENT_H
#define UART_EVENT_H

/* USART peripheral configuration defines */
#define BT_UART                           USART2
#define BT_UART_CLK                       RCC_APB1Periph_USART2
#define BT_UART_IRQn                      USART2_IRQn

#define BT_UART_GPIO_PORT                 GPIOA
#define BT_UART_GPIO_CLK                  RCC_AHB1Periph_GPIOA
#define BT_UART_AF                        GPIO_AF_USART2

#define BT_UART_TX_PIN                    GPIO_Pin_2
#define BT_UART_TX_SOURCE                 GPIO_PinSource2

#define BT_UART_RX_PIN                    GPIO_Pin_3
#define BT_UART_RX_SOURCE                 GPIO_PinSource3

#define BT_UART_BAUDRATE                  115200
#define UART_BUFFERSIZE          	      256

void uart_init( void );
void bt_uart_config( void );
void uart_send_byte(uint8_t ui8_byte);
uint8_t uart_read_byte( void );

#define bt_uart_isr                       USART2_IRQHandler

#endif //UART_EVENT_H
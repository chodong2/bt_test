#include "global_define.h"

#ifdef ENABLE_HAPTIC

#ifndef ENABLE_DBG
static const GPIO_InitTypeDef HAPTIC_GpioConfiguration  =  {PIN_HAPTIC_GPIO, GPIO_Mode_OUT,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};
#endif //ENABLE_DBG

void haptic_init( void )
{
	haptic_config_gpio();
	
	HAPTIC_IDLE;
}

void haptic_config_gpio( void )
{

  /* GPIOA clock enable */
	RCC_AHB1PeriphClockCmd(HAPTIC_GPIO_CLK, ENABLE);

	GPIO_Init(PWM_HAPTIC_GPIO_PORT, (GPIO_InitTypeDef *)&HAPTIC_GpioConfiguration );

}



#endif //ENABLE_HAPTIC
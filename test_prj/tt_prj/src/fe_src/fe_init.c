#include "global_define.h"
#ifdef ENABLE_FORMANT_ENHANCEMENT
#include "enh_formant.h"
#include "Common.h"
#include "AHDR.h"

void fe_init()
{
	Init_fe();
	init_vad();
	reset_AHDR();

	for (int j = 0; j < AUDIO_FRAME_SIZE; j++)
	{
		g_str_fe.ui16_data[j] = 0;
		g_str_fe.ui16_data2[j] = 0;
	}

	g_str_bitflag.b1_fe_enable_flag = 0;
	g_str_fe.ui16_frame_no = 0;
}

void formant_enhancement( void )
{
	int16_t i16_buf16[AUDIO_FRAME_SIZE];

	g_str_fe.ui16_frame_no++;

	for (int j = 0; j < AUDIO_FRAME_SIZE; j++)
	{
		i16_buf16[j] = g_str_audio.ui16_save_buf[j];
	}

	FormantEnhancement(i16_buf16, 2);

	if (AcousticHighDynamicRange_exe(i16_buf16, AUDIO_FRAME_SIZE) != NO_ERROR)
	{
		for (int j = 0; j < AUDIO_FRAME_SIZE; j++)
		{
			if(   g_str_fe.ui16_frame_no % 2 )
			{
				g_str_fe.ui16_data2[j<<1] = 0;
			}
			else
			{
				g_str_fe.ui16_data[j<<1] = 0;
			}
		}
	}

	for (int j = 0; j < AUDIO_FRAME_SIZE; j++)
	{
		if( g_str_fe.ui16_frame_no % 2 )
		{
			g_str_fe.ui16_data[j<<1] = (uint16_t)i16_buf16[j];
			g_str_fe.ui16_data[(j<<1)+1] = (uint16_t)i16_buf16[j];
		}
		else
		{
			g_str_fe.ui16_data2[j<<1] = (uint16_t)i16_buf16[j];
			g_str_fe.ui16_data2[(j<<1)+1] = (uint16_t)i16_buf16[j];
		}
	}
}
#endif
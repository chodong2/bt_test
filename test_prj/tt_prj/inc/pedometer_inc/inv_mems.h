/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#ifndef _INV_IVORY_EXPORT_HEADERS_H__
#define _INV_IVORY_EXPORT_HEADERS_H__

void inv_mems_init(void);
void inv_mems_get_data(void);
void pedometer(void);

#endif // _INV_IVORY_EXPORT_HEADERS_H__

/*
* ________________________________________________________________________________________________________
* Copyright � 2014 InvenSense Inc.  All rights reserved.
*
* This software and/or documentation  (collectively �Software�) is subject to InvenSense intellectual property rights 
* under U.S. and international copyright and other intellectual property rights laws.
*
* The Software contained herein is PROPRIETARY and CONFIDENTIAL to InvenSense and is provided 
* solely under the terms and conditions of a form of InvenSense software license agreement between 
* InvenSense and you and any use, modification, reproduction or disclosure of the Software without 
* such agreement or the express written consent of InvenSense is strictly prohibited.
*
* EXCEPT AS OTHERWISE PROVIDED IN A LICENSE AGREEMENT BETWEEN THE PARTIES, THE SOFTWARE IS 
* PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
* TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
* EXCEPT AS OTHERWISE PROVIDED IN A LICENSE AGREEMENT BETWEEN THE PARTIES, IN NO EVENT SHALL 
* INVENSENSE BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, OR ANY
* DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
* NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
* OF THE SOFTWARE.
* ________________________________________________________________________________________________________
*/

#ifndef _DMP_3_DEFAULT_20648_XFSD_H__
#define _DMP_3_DEFAULT_20648_XFSD_H__

//#include "mltypes.h"

#ifdef __cplusplus
extern "C"
{
#endif

int inv_load_firmware_20648(void);
void inv_get_dmp_start_address_20648(unsigned short *dmp_cnfg);
int dmp_reset_control_registers_20648(void);
int dmp_set_data_output_control1_20648(int output_mask);
int dmp_set_data_output_control2_20648(int output_mask);
int dmp_set_data_interrupt_control_20648(uint32_t interrupt_ctl);
int dmp_set_FIFO_watermark_20648(unsigned short fifo_wm);
int dmp_set_data_rdy_status_20648(unsigned short data_rdy);
int dmp_set_motion_event_control_20648(unsigned short motion_mask);
int dmp_set_sensor_rate_20648(int sensor, short divider);
int dmp_set_batchmode_params_20648(unsigned int thld, short mask);
int dmp_set_bias_20648(int *bias);
int dmp_get_bias_20648(int *bias);
int dmp_set_gyro_sf_20648(long gyro_sf);
int dmp_set_accel_feedback_gain_20648(int accel_gain);
int dmp_set_accel_cal_params_20648(int *accel_cal);
int dmp_set_compass_cal_params_20648(int *compass_cal);
int dmp_set_compass_matrix_20648(int *compass_mtx);
int dmp_get_pedometer_num_of_steps_20648(uint32_t *steps);
int dmp_set_pedometer_rate_20648(int ped_rate);
int dmp_set_wom_enable_20648(unsigned char enable);
int dmp_set_wom_motion_threshold_20648(int threshold);
int dmp_set_wom_time_threshold_20648(unsigned short threshold);
int dmp_set_accel_fsr_20648(short accel_fsr);
int dmp_set_accel_scale2_20648(short accel_fsr);
int dmp_set_eis_auth_input_20648(long eis_auth_input);
int dmp_get_eis_auth_output_20648(long *eis_auth_output);


#ifdef __cplusplus
}
#endif

// _DMP_3_DEFAULT_20648_XFSD_H__
#endif

#ifndef DBG_CODE_H
#define DBG_CODE_H

/* USART peripheral configuration defines */
#define DBG_UART                           USART1
#define DBG_UART_CLK                       RCC_APB2Periph_USART1
#define DBG_UART_IRQn                      USART1_IRQn

#define DBG_UART_GPIO_PORT                 GPIOA
#define DBG_UART_GPIO_CLK                  RCC_AHB1Periph_GPIOA
#define DBG_UART_AF                        GPIO_AF_USART1

#define DBG_UART_TX_PIN                    GPIO_Pin_9
#define DBG_UART_TX_SOURCE                 GPIO_PinSource9

#define DBG_UART_RX_PIN                    GPIO_Pin_10
#define DBG_UART_RX_SOURCE                 GPIO_PinSource10

#define DBG_UART_BAUDRATE                  115200

void dbg_init( void );
void dbg_uart_config( void );
void dbg_send_byte(uint8_t ui8_byte);
uint8_t dbg_read_byte( void );
void dbg_parser( void );
void dbg_print( uint8_t byte[] );

#define dbg_uart_isr                       USART1_IRQHandler

#endif //DBG_CODE_H
#ifndef GLOBAL_DEFINE_H
#define GLOBAL_DEFINE_H


/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */

#define	FLASH_ADDR_JUMP_APP		ADDR_FLASH_SECTOR_2
#define	FLASH_ADDR_START_APP1	ADDR_FLASH_SECTOR_3
#define	FLASH_ADDR_START_APP2	ADDR_FLASH_SECTOR_5
#define	FLASH_ADDR_END_APP2		ADDR_FLASH_SECTOR_6

#define FLASH_ADDR_START_PEDO   ADDR_FLASH_SECTOR_6   /* Start @ of user Flash area */
#define FLASH_ADDR_END_PEDO     ADDR_FLASH_SECTOR_7   /* End @ of user Flash area */


#if defined(CURRENT_APP_1)
	#define	FLASH_ADDR_CURRENT_APP	FLASH_ADDR_START_APP1
	#define	FLASH_ADDR_UPDATE_APP	FLASH_ADDR_START_APP2
#endif

#if defined(CURRENT_APP_2)
	#define	FLASH_ADDR_CURRENT_APP	FLASH_ADDR_START_APP2
	#define	FLASH_ADDR_UPDATE_APP	FLASH_ADDR_START_APP1
#endif

#if defined(CURRENT_APP)
	#define	FLASH_ADDR_CURRENT_APP	ADDR_FLASH_SECTOR_0
	#define	FLASH_ADDR_UPDATE_APP	FLASH_ADDR_START_APP2
#endif


#if defined(CURRENT_APP_1)
	#define ERASE_START_SECTOR	FLASH_Sector_5
	#define ERASE_END_SECTOR	FLASH_Sector_6

#endif

#if defined(CURRENT_APP_2)

	#define ERASE_START_SECTOR	FLASH_Sector_3
	#define ERASE_END_SECTOR	FLASH_Sector_5	

#endif

#if defined(CURRENT_APP)
	#define ERASE_START_SECTOR	FLASH_Sector_5
	#define ERASE_END_SECTOR	FLASH_Sector_6

#endif

/* pre define module */
//#define ENABLE_WATCHDOG

#define ENABLE_IS2063
#define ENABLE_UART
#define ENABLE_AUDIO
#define ENABLE_HAPTIC
#define ENABLE_PWM

//#define ENABLE_OTA

#define ENABLE_BATT_STATUS
#define ENABLE_NOTIFY

#define ENABLE_LOWPWR
//#define ENABLE_FORMANT_ENHANCEMENT
#define ENABLE_PEDOMETER
//#define ENABLE_AUTH

//#define ENABLE_DBG

#define ENABLE_BATTLOW

#ifndef ENABLE_BATT_STATUS
	#undef ENABLE_BATTLOW
#endif

#define CES_DEMOCODE

#ifdef ENABLE_HAPTIC
	#define HAPTIC_ACTIVE					GPIOA->BSRRL = PIN_HAPTIC_GPIO
	#define HAPTIC_IDLE						GPIOA->BSRRH = PIN_HAPTIC_GPIO
#else
	#define HAPTIC_ACTIVE
	#define HAPTIC_IDLE
#endif


#ifdef ENABLE_IS2063
	#include "bt_app.h"
	#include "bt_command_send.h"
	#include "bt_command_decode.h"

	#include "uart_parser.h"
#else
	#undef ENABLE_OTA
#endif //ENABLE_IS2063

/* include header files */
#include "stm32f4xx.h"

#include "pin_enable.h"
#include "key_event.h"
#include "key_scan.h"
#include "timer_event.h"

#ifdef ENABLE_UART
	#include "uart_event.h"
#endif //ENABLE_UART

#ifdef ENABLE_LED
	#include "led_event.h"
#endif //ENABLE_LED

#ifdef ENABLE_PWM
	#include "pwm_event.h"
	#include "led_pattern.h"
#endif //ENABLE_PWM

#ifdef ENABLE_AUDIO
	#include "audio_event.h"
#endif //ENABLE_AUDIO

#ifdef ENABLE_FORMANT_ENHANCEMENT
	#include "fe_init.h"
#endif //ENABLE_FORMANT_ENHANCEMENT

#ifdef ENABLE_HAPTIC
	#include "haptic_event.h"
#endif //ENABLE_HAPTIC

#ifdef ENABLE_DBG
	#include "dbg_code.h"
#endif //ENABLE_DBG

#ifdef ENABLE_OTA
	#include "ota_event.h"
#endif //ENABLE_OTA

#ifdef ENABLE_LOWPWR
	#include "low_power.h"
	#include "tm_stm32f4_rcc.h"
#endif //ENABLE_LOWPWR


#ifdef ENABLE_PEDOMETER
        #include "inv_defines.h"
        #include "flash.h"
#endif //ENABLE_PEDOMETER
#ifdef ENABLE_WATCHDOG
	#include "tm_stm32f4_watchdog.h"
#endif //ENABLE_WATCHDOG

#include "struct.h"

#endif //GLOBAL_DEFINE_H

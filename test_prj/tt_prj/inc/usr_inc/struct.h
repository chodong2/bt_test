#ifndef STRUCT_H
#define STRUCT_H

#include "global_define.h"

typedef enum device_status_{

	STATUS_STAND_BY			 = 0x0,

	STATUS_INCOMING_CALL	 = 0x1,
	STATUS_INCOMING_MSG	 = 0x2,
	STATUS_INCOMING_NOTI	 = 0x4,
	STATUS_IN_NOTIFY		 = 0x7, // STATUS_INCOMING_CALL | STATUS_INCOMING_MSG | STATUS_INCOMING_NOTI

	STATUS_ACTIVE_CALL_TTS	 = 0x8,

}device_status_t;

typedef enum freq_status_{

	FREQ_CHK_READY			 = 0x0,
	FREQ_CHK_START			 = 0x1,
	FREQ_CHK_ING			 = 0x2,
    FREQ_CHK_ANALYZE                 = 0x3,
	FREQ_CHK_END			 = 0x4,

}freq_status_t;

typedef enum power_mode_{

	PWR_MODE_NONE			 = 0x0,
	PWR_MODE_LOW			 = 0x1,
	PWR_MODE_HIGH			 = 0x2,
	
}power_mode_t;

typedef enum pairkey_status_{

	PAIR_NONE				 = 0x0,
	PAIR1_PUSH				 = 0x1,
	PAIR2_PUSH				 = 0x2,
	PAIR_MODE				 = 0x3,

}pairkey_status_t;

typedef enum audio_state_{

	AUDIO_STATE_STOP = 0,
	AUDIO_STATE_PLAY = 1,

}audio_state_t;

typedef enum audio_frame_{

	AUDIO_FRAME_OK = 0,
	AUDIO_FRAME_ERR = 1,

}audio_frame_t;

typedef struct bitflag_struct_{

	volatile audio_frame_t b1_frame_state:1;
	volatile uint16_t b1_power_key_flag:1;
	volatile uint16_t b1_power_down:1;
	volatile uint16_t b1_uart_end:1;
	
	volatile audio_state_t b1_audio_rx_state:1;
	volatile audio_state_t b1_audio_tx_state:1;
	volatile uint16_t b1_audio_stream_flag:1;
	
#ifdef ENABLE_TEST
	volatile uint16_t b1_pwm_test:1;
	volatile uint16_t b1_led_test:1;
	volatile uint16_t b1_haptic_test:1;
#endif
	
	volatile uint16_t b1_bt_conn:1;

#ifdef ENABLE_FORMANT_ENHANCEMENT
	volatile uint16_t b1_fe_enable_flag:1;
#endif

	volatile uint16_t b1_dimming_flag:1;

	volatile pairkey_status_t b2_pair_flag:2;
	
	volatile freq_status_t b2_freq_chk:2;
	volatile device_status_t b4_device_status:4;
	volatile uint16_t b1_incoming_call:1;
	volatile uint16_t b1_audio_play:1;
	volatile uint16_t b1_batt_chk:1;

	//SH_ADD
	volatile uint16_t b1_power_mode_change:1;
	volatile power_mode_t b2_power_mode:2;

	volatile uint16_t b1_motion_chk:1;
	
#ifdef ENABLE_DBG
	volatile uint16_t b1_dbg_parsing:1;
	volatile uint16_t b1_dbg_enable:1;
	volatile uint16_t b3_dbg_mode:3;
#endif //ENABLE_DBG

}bitflag_struct_t;

extern bitflag_struct_t g_str_bitflag;

typedef struct timer_struct_{

	volatile uint16_t ui16_audio_cnt;
	volatile uint16_t ui16_power_cnt;
	volatile uint16_t ui16_led_cnt;
	
	volatile uint16_t ui16_sleep_cnt;
    volatile uint16_t ui16_haptic_cnt;

}timer_struct_t;

extern timer_struct_t g_str_timer;

typedef struct pwm_struct_{
	
	volatile uint16_t ui16_trigger;
	volatile uint16_t ui16_dimming_flag;
	volatile uint16_t ui16_dimming_data;
	volatile uint16_t ui16_pwm;

}pwm_struct_t;

typedef enum audio_channel_{

	AUDIO_CHANNEL_NONE = 0,
	AUDIO_CHANNEL_LEFT = 0,
	AUDIO_CHANNEL_RIGHT = 1,
	AUDIO_CHANNEL_TOGGLE = 1,

}audio_channel_t;

typedef enum audio_input_freq_{

	AUDIO_INPUT_NONE = I2S_AudioFreq_8k,
	AUDIO_INPUT_8K = I2S_AudioFreq_8k,
	AUDIO_INPUT_16K = I2S_AudioFreq_16k,
	AUDIO_INPUT_44_1K = I2S_AudioFreq_44k,
	AUDIO_INPUT_48K = I2S_AudioFreq_48k,

}audio_input_freq_t;

#ifdef ENABLE_AUDIO

typedef struct audio_struct_{

	volatile uint16_t ui16_save_buf[AUDIO_FRAME_SIZE];
	volatile uint16_t ui16_save_cnt;
	volatile uint16_t ui16_freq_cnt;
  	volatile uint16_t ui16_sec_cnt;

	volatile audio_input_freq_t	ui16_pre_freq;
	volatile audio_channel_t ui16_channel_sel;
        
	volatile audio_input_freq_t ui16_audio_type;
  
}audio_struct_t;

extern audio_struct_t g_str_audio;

#ifdef ENABLE_FORMANT_ENHANCEMENT
typedef struct fe_struct_{

	volatile uint16_t ui16_frame_no;
	volatile uint16_t ui16_inputdata[AUDIO_FRAME_SIZE];
	volatile uint16_t ui16_data[AUDIO_FRAME_SIZE<<1];
	volatile uint16_t ui16_data2[AUDIO_FRAME_SIZE<<1];

}fe_struct_t;

extern fe_struct_t g_str_fe;
#endif //ENABLE_FORMANT_ENHENCEMENT

#endif //ENABLE_AUDIO

#ifdef ENABLE_UART
typedef struct bt_uart_struct_{
  
	volatile uint8_t	ui8_rx_buf[UART_BUFFERSIZE];
	volatile uint16_t	ui16_rx_head;
	volatile uint16_t	ui16_rx_tail;
	volatile uint16_t	ui16_rx_cnt;
	volatile uint16_t	ui16_rx_flag;
	
	volatile uint8_t	ui8_tx_buf[UART_BUFFERSIZE];
	volatile uint16_t	ui16_tx_head;
	volatile uint16_t	ui16_tx_tail;
	volatile uint16_t	ui16_tx_cnt;
	volatile uint16_t	ui16_tx_flag;
  
}bt_uart_struct_t;

extern bt_uart_struct_t g_str_bt;

#endif //ENABLE_UART

#ifdef ENABLE_DBG
typedef struct dbg_uart_struct_{
	volatile uint16_t	ui16_rx_cnt;
	volatile uint16_t	ui16_tx_cnt;
	
	volatile uint16_t	ui16_tx_size;	
  
	volatile uint8_t	ui8_rx_buf[UART_BUFFERSIZE];
	volatile uint8_t	ui8_tx_buf[UART_BUFFERSIZE];
}dbg_uart_struct_t;

extern dbg_uart_struct_t g_str_dbg;

#endif //ENABLE_DBG

#ifdef ENABLE_OTA

typedef enum ota_state_{

	OTA_STATE_NONE			= 0,
	OTA_STATE_INITIAL		= 1,
	OTA_STATE_CHECK_VERSION	= 2,
	OTA_STATE_CHECK_SIZE	= 3,
	OTA_STATE_UPDATE		= 4,
	OTA_STATE_DATA_END		= 5,
	OTA_STATE_SUCCESS		= 6,
	OTA_STATE_FAIL			= 7,
	OTA_STATE_END			= 8,
	OTA_STATE_READY			= 9,

}ota_state_t;


typedef struct ota_struct_{
	volatile ota_state_t ui16_state;
}ota_struct_t;

extern ota_struct_t g_str_ota;

#endif //ENABLE_OTA

#ifdef ENABLE_BATT_STATUS


typedef enum charger_status_{

	CHG_STATUS_NO_PLUG				= 0x00,
	CHG_STATUS_IN_CHARGING			= 0x01,
	CHG_STATUS_CHARGING_COMPLETE	= 0x02,
	CHG_STATUS_CHARGING_FAIL		= 0x03,
	CHG_STATUS_TYPE_REPORT			= 0x04,

	CHG_STATUS_INITIAL				= 0xFF,

}charger_status_t;

typedef enum charger_type_{

	CHG_TYPE_UNKNOWN	= 0x00,
	CHG_TYPE_NON_DCD	= 0x01,
	CHG_TYPE_SDP		= 0x02,
	CHG_TYPE_DCP		= 0x03,
	CHG_TYPE_CDP		= 0x04,
	CHG_TYPE_SONY		= 0x05,
	CHG_TYPE_APPLE_2_5W	= 0x06,
	CHG_TYPE_APPLE_5W	= 0x07,
	CHG_TYPE_APPLE_10W	= 0x08,
	CHG_TYPE_APPLE_12W	= 0x09,

	CHG_TYPE_INITIAL	= 0xFF,

}charger_type_t;

typedef struct chg_struct_{
	
	volatile charger_status_t ui16_chg_status;
	volatile charger_type_t ui16_chg_type;

}chg_struct_t;

extern chg_struct_t g_str_chg;

typedef enum battery_status_{

	BATT_STATUS_DANGER				= 0x00,
	BATT_STATUS_LOW					= 0x01,
	BATT_STATUS_NORMAL				= 0x02,
	BATT_STATUS_HIGH				= 0x03,
	BATT_STATUS_FULL				= 0x04,
	BATT_STATUS_IN_CHARGING			= 0x05,
	BATT_STATUS_CHARGING_COMPLETE	= 0x06,
	
	BATT_STATUS_INITIAL				= 0xFF,

}battery_status_t;

typedef enum battery_level_{

	BATT_LEVEL_3_0 = 0x00,
	BATT_LEVEL_3_1 = 0x01,
	BATT_LEVEL_3_2 = 0x02,
	BATT_LEVEL_3_3 = 0x03,
	BATT_LEVEL_3_4 = 0x04,
	BATT_LEVEL_3_5 = 0x05,
	BATT_LEVEL_3_6 = 0x06,
	BATT_LEVEL_3_7 = 0x07,
	BATT_LEVEL_3_8 = 0x08,
	BATT_LEVEL_3_9 = 0x09,
	BATT_LEVEL_4_0 = 0x0A,
	BATT_LEVEL_4_1 = 0x0B,
	BATT_LEVEL_4_2 = 0x0C,
	
	BATT_LEVEL_INITIAL = 0xFF,

}battery_level_t;

typedef struct batt_struct_{
	
	volatile battery_status_t ui16_batt_status;
	volatile battery_level_t ui16_batt_level;

}batt_struct_t;

extern batt_struct_t g_str_batt;

#endif //ENABLE_BATT_STATUS

#ifdef ENABLE_NOTIFY

typedef struct pattern_cnt_struct_{

	volatile uint16_t ui16_timer_cnt;
	
	pwm_struct_t str_pwm;
  
}pattern_cnt_struct_t;

extern pattern_cnt_struct_t g_str_batt_level;
extern pattern_cnt_struct_t g_str_app_noti;

extern pattern_cnt_struct_t g_str_bt_conn;
extern pattern_cnt_struct_t g_str_bt_disconn;

extern pattern_cnt_struct_t g_str_pwr_on;

extern pattern_cnt_struct_t g_str_incoming_call;

#endif //ENABLE_NOTIF

#endif //STRUCT_H

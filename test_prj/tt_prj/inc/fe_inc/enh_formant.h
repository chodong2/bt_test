#ifndef ENH_FORMANT_H
#define ENH_FORMANT_H

#include "levinson.h"
#include "vad.h"

#define EQNSIZE              10
#define WINDOW_BY_TWO        (FFTSIZE>>1)
#define FORMANTNUM           10
#define LOOPMAX              5000


void Init_fe();
void Apply_Window(float *buf, const float *w, short size);
void HammingGainFilter(float BW, float *GainFilter, float GainSize);
void bairstow(float *equ, float *arroot_R, float arroot_I[][2], float Accuracy);
void SortArray(float arroot_I[][2]);
void SortArraySingle(float *Formants, float *BandWidth, int Size);
void FormantEnhancement(Word16 *Signal, int Mode);

extern float Previn_Buf[FFTSIZE];
extern float Prevsynth_Buf[FFTSIZE];

#endif

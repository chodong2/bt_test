#ifndef _HAPTIC_EVENT_H_
#define _HAPTIC_EVENT_H_

#define PIN_HAPTIC_GPIO					GPIO_Pin_8

#define HAPTIC_GPIO_CLK					RCC_AHB1Periph_GPIOA

void haptic_init(void);
void haptic_config_gpio(void);

#endif //_HAPTIC_EVENT_H_
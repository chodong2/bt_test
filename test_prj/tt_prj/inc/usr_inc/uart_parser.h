#ifndef UART_PARSER_H
#define UART_PARSER_H

#include <stdio.h>

void uart_parser(uint8_t* u8_data, uint8_t u8_size);
void received_data(uint16_t u16_notify, uint16_t u16_index);

#endif //UART_PARSER_H
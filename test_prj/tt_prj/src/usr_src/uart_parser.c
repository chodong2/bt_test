#include "global_define.h"

#ifdef ENABLE_UART
#ifdef ENABLE_IS2063

typedef enum {
	RX_PARSER_INITIAL,
	RX_PARSER_LENGTH,
	RX_PARSER_DATAFLAG,
	RX_PARSER_DATA,
	RX_PARSER_SUBDATA_FLAG,
	RX_PARSER_SUBDATA,
	RX_PARSER_END,
} RX_PARSER_MODE;

enum {
	RECEIVED_BT_CONN			= 12,
  
	RECEIVED_INCOMING_CALL = 13,
	RECEIVED_RECEIVED_CALL		= 14,
	RECEIVED_END_CALL			= 15,
	RECEIVED_APP_NOTIF			= 16,
	RECEIVED_HEALTH_DATA		= 17,
	RECEIVED_LED_CONTROL		= 18,
	RECEIVED_BATT_LEVEL			= 19,

#ifdef ENABLE_PEDOMETER
	RECEIVED_HEALTH_TIME_SYNC	= 20,//1A��A��������aA��
	RECEIVED_HEALTH_REQ			= 21,//2A��(��CA|)A��������aA��
#endif //ENABLE_PEDOMETER

	RECEIVED_OUTGOING_CALL		= 22,
	RECEIVED_MUSIC_PLAY			= 23,
	RECEIVED_MUSIC_STOP			= 24,
	RECEIVED_OTA_CONTROL		= 55,
} ;

enum {
	BT_CONNECT					= 0,
	BT_DISCONNECT				= 1,
} ;

enum {
	HEALTH_WALK_COMPLETE		= 0,
	HEALTH_ACTIVITY_COMPLETE	= 1,
	HEALTH_MOVETIME				= 2,
} ;

enum {
	NOTIFY_LED_DISABLE			= 0,
	NOTIFY_LED_ENABLE			= 1,
} ;

enum {
	PEDO_DEMO_STOP = 0,
	PEDO_DEMO_START = 1,
};

static RX_PARSER_MODE	RX_ParserState;

void uart_parser(uint8_t* u8_data, uint8_t u8_size)
{
	uint8_t u8_current_data = u8_data[0];
	uint8_t u8_length = 0;
	uint16_t u16_notify = 0;
	uint16_t u16_index = 0;
	
	if( u8_current_data == '@' )
	{
#ifdef ENABLE_OTA
		ota_parser( u8_data, u8_size );
#endif
		return;
	}
	
	RX_ParserState = RX_PARSER_INITIAL;
	
	for( int i = 0; i < u8_size; i++ )
	{
		u8_current_data = u8_data[i];
		
		switch(RX_ParserState)
		{
		case RX_PARSER_INITIAL :
			if(u8_current_data == '$')
			{
				RX_ParserState = RX_PARSER_LENGTH;
			}
			break;

		case RX_PARSER_LENGTH:
			if (isdigit(u8_current_data))
			{
				if (isdigit(u8_data[i + 1]))
				{
					u8_length = (u8_current_data - '0') * 10 + u8_data[++i] - '0';
				}
				else if (u8_data[i + 1] == '$')
				{
					u8_length = u8_current_data;
				}
			}

			RX_ParserState = RX_PARSER_DATAFLAG;
			break;
		
		case RX_PARSER_DATAFLAG :
			if(u8_current_data == '$')
			{
				RX_ParserState = RX_PARSER_DATA;
			}
			else
			{
				RX_ParserState = RX_PARSER_INITIAL;
			}
			break;

		case RX_PARSER_DATA :
			u16_notify = (u8_current_data-'0')*10+u8_data[++i]-'0';
			if( u8_length == '3' )
			{
				RX_ParserState = RX_PARSER_END;
			}
			else
			{
				RX_ParserState = RX_PARSER_SUBDATA_FLAG;
			}
			break;

		case RX_PARSER_SUBDATA_FLAG :
		  	if(u8_current_data == '$')
			{
				RX_ParserState = RX_PARSER_SUBDATA;
			}
			else
			{
				RX_ParserState = RX_PARSER_INITIAL;
			}
			break;
			
		case RX_PARSER_SUBDATA :
#ifdef ENABLE_PEDOMETER
			if ((u16_notify == RECEIVED_HEALTH_TIME_SYNC) || (u16_notify == RECEIVED_HEALTH_REQ))//pedometer
			{
				g_ui32_date = ((u8_current_data - '0') * 100000) + ((u8_data[++i] - '0') * 10000);//year
				g_ui32_date += ((u8_data[++i] - '0') * 1000);//month1
				g_ui32_date += ((u8_data[++i] - '0') * 100);//month2
				g_ui32_date += ((u8_data[++i] - '0') * 10);//day1
				g_ui32_date += (u8_data[++i] - '0');//day2
				if (u8_data[++i] == '$')
				{
					g_ui16_hour = (u8_data[++i] - '0') * 10;//10's digit hour
					g_ui16_hour += u8_data[++i] - '0';//1's digit hour
					if (u16_notify == RECEIVED_HEALTH_TIME_SYNC)
						BT_SendSPPData((uint8_t *)u8_data, u8_size, 0);
				}
				else
					RX_ParserState = RX_PARSER_INITIAL;
			}
			else
#endif
			{
				u16_index = u8_current_data - '0';
			}
			RX_ParserState = RX_PARSER_END;
			break;
		  
		default :
			RX_ParserState = RX_PARSER_INITIAL;
			break;

		}
		
		if( RX_ParserState == RX_PARSER_END )
		{
			received_data( u16_notify, u16_index );
			RX_ParserState = RX_PARSER_INITIAL;
		}

	}
}

void received_data(uint16_t u16_notify, uint16_t u16_index)
{
	switch( u16_notify )
	{
	case RECEIVED_BT_CONN :
		switch(u16_index)
		{
		case BT_CONNECT :
#ifdef ENABLE_NOTIFY
			g_str_bt_conn.ui16_timer_cnt = 350;
#endif //ENABLE_NOTIFY
			break;
		
		case BT_DISCONNECT :
#ifdef ENABLE_NOTIFY
			g_str_bt_disconn.ui16_timer_cnt = 350;  
#endif //ENABLE_NOTIFY
			break;

		default :
			break;
			  
		}  
		break;  
	  
	case RECEIVED_INCOMING_CALL:
		g_str_bitflag.b1_incoming_call = 1;
		break;
	
	case RECEIVED_RECEIVED_CALL :
		g_str_bitflag.b1_incoming_call = 0;
		HAPTIC_IDLE;
		
#ifdef ENABLE_PWM
		pwm_pulse_off();
#endif
		
#ifdef ENABLE_LOWPWR
		set_clk_high();
#endif
		break;
	
	case RECEIVED_END_CALL :
		g_str_bitflag.b1_incoming_call = 0;
		HAPTIC_IDLE;

#ifdef ENABLE_PWM
		pwm_pulse_off();
#endif

#ifdef ENABLE_LOWPWR
		set_clk_low();
#endif
		break;
	
	case RECEIVED_APP_NOTIF :
#ifdef ENABLE_NOTIFY
		g_str_app_noti.ui16_timer_cnt = 480;
#endif //ENABLE_NOTIFY
		break;
	
	case RECEIVED_HEALTH_DATA :
		
		switch(u16_index)
		{
		case HEALTH_WALK_COMPLETE :
			break;
		
		case HEALTH_ACTIVITY_COMPLETE :
			break;
		
		case HEALTH_MOVETIME :
			break;
		
		default :
			break;
			  
		}
		break;
	case RECEIVED_LED_CONTROL :
	
		switch(u16_index)
		{
		case NOTIFY_LED_ENABLE :
			break;
		
		case NOTIFY_LED_DISABLE :
			break;
		
		default :
			break;  
		}
		break;
		
	case RECEIVED_BATT_LEVEL:
#ifdef ENABLE_NOTIFY
		g_str_batt_level.ui16_timer_cnt = 350;
#endif //ENABLE_NOTIFY
		g_str_timer.ui16_sleep_cnt = 0;

#ifdef ENABLE_NOTIFY
                uint8_t byte[10] = "";

                sprintf(byte, "$6$19$%d", (int)(g_str_batt.ui16_batt_level + 30));
                BT_SendSPPData(byte, 8, 0);
#endif //ENABLE_NOTIFY
		break;

#ifdef ENABLE_PEDOMETER
	case RECEIVED_HEALTH_TIME_SYNC://20 ��A�ơ� �좯��aE��

		g_ui32_pedometer_timer = 0;
		g_ui32_pedometer_call_app = 1;
		g_ui32_app_date = g_ui32_date;
		g_ui16_app_hour = g_ui16_hour;

		if (g_ui32_mcu_hour)
		{
			g_ui32_mcu_hour = 0;
			g_ui32_pedometer_call_app = 0;
		}
		break;
	case RECEIVED_HEALTH_REQ://21
		g_ui32_pedometer_call_app = 2;
		break;
#endif //ENABLE_PEDOMETER

	case RECEIVED_MUSIC_PLAY:
		g_str_bitflag.b1_incoming_call = 0;
		HAPTIC_IDLE;
#ifdef ENABLE_PWM
		pwm_pulse_off();
#endif
	  	break;
		
	case RECEIVED_MUSIC_STOP:
		g_str_bitflag.b1_incoming_call = 0;
		HAPTIC_IDLE;
#ifdef ENABLE_PWM
		pwm_pulse_off();
#endif
		break;
		
	default :
		break;
	}
}

#endif //ENABLE_IS2063

#endif //ENABLE_UART

